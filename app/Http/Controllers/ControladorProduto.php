<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use App\Produto;

class ControladorProduto extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){        
        $prods = Produto::all();
        return json_encode($prods);
    }

    public function indexView()
    {
        $produtos = Produto::all();
        foreach ($produtos as $produto) {
            
            $cat = Categoria::find($produto->categoria_id);
            $produto->categoria_id = $cat->nome;
        }

        return view('produtos', compact('produtos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cats = Categoria::all();
        return view('novoProduto', compact('cats'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeView(Request $request)
    {
        //
        $produto = new Produto();
        $produto->nome = $request->input('nomeProduto');
        $produto->estoque = $request->input('estoqueProduto');
        $produto->preco = $request->input('precoProduto');
        $produto->categoria_id = $request->input('categoriaProduto');
        $produto->save();

        return redirect('/produtos');
    }

    public function store(Request $request)
    {
        //
        $produto = new Produto();
        $produto->nome = $request->input('nome');
        $produto->estoque = $request->input('estoque');
        $produto->preco = $request->input('preco');
        $produto->categoria_id = $request->input('categoria_id');
        $produto->save();

        return json_encode($produto);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
    
        $produto = Produto::find($id);
        $cats = Categoria::all();
        if(isset($produto))
            return view('editarProduto', compact(['produto', 'cats']));
        return redirect('/produtos');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $produto = Produto::find($id);
        if(isset($produto)){
            $produto->nome = $request->input('nomeProduto');
            $produto->estoque = $request->input('estoqueProduto');
            $produto->preco = $request->input('precoProduto');
            $produto->categoria_id = $request->input('categoriaProduto');
            $produto->save();
        }
        return redirect('/produtos') ;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produto = Produto::find($id);
        if(isset($produto)){
            $produto->delete();
        }
        return redirect('/produtos');
    }
}
