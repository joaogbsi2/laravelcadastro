@extends('layout.app', ["current" => "produtos"])

@section('body')
	<div class="card border">
		<div class="card-body">
			<form action="/produtos" method="POST">
				@csrf
				<div class="form-group">
					<label for="nomeProduto">Nome do Produto</label>
					<input type="text" class="form-control" name="nomeProduto" id="nomeProduto" placeholder="Produto">
					<label for="estoqueProduto">estoque do Produto</label>
					<input type="text" class="form-control" name="estoqueProduto" id="estoqueProduto" placeholder="Estoque">
					<label for="precoProduto">estoque do Produto</label>
					<input type="text" class="form-control" name="precoProduto" id="precoProduto" placeholder="Preco">
					<div class="form-row align-items-center">
					    <div class="col-auto my-1">
					    	<label class="mr-sm-2" for="categoriaProduto">Categoria</label>
					    	<select class="custom-select mr-sm-2" name="categoriaProduto" id="categoriaProduto">
					    		<option selected>Choose...</option>
					    		@foreach($cats as $cat)
					    			<option value="{{ $cat->id }}">{{ $cat->nome }}</option>
					    		@endforeach					    	
					    	</select>
					    </div>
				</div>
				<button type="submit" class="btn btn-primary btn-sn">Salvar</button>
				<button type="submit" class="btn btn-danger btn-sn">Cancel</button>
			</form>
		</div>
	</div>

@endsection