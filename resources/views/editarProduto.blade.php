
@extends('layout.app', ["current" => "produtos"])

@section('body')
	<div class="card border">
		<div class="class-body">
			<form action="/produtos/{{ $produto->id }}" method="POST">
				@csrf
				<div class="form-grup">
					<label for="nomeProduto">Nome do Produto</label>
					<input type="text" class="form-control" name="nomeProduto" id="nomeProduto" placeholder="Produto" value="{{ $produto->nome }}">
					<label for="estoqueProduto">estoque do Produto</label>
					<input type="text" class="form-control" name="estoqueProduto" id="estoqueProduto" placeholder="Estoque" value="{{ $produto->estoque }}">
					<label for="precoProduto">estoque do Produto</label>
					<input type="text" class="form-control" name="precoProduto" id="precoProduto" placeholder="Preco" value="{{ $produto->preco }}">
					<div class="form-row align-items-center">
					    <div class="col-auto my-1">
					    	<label class="mr-sm-2" for="categoriaProduto">Categoria</label>
					    	<select class="custom-select mr-sm-2" name="categoriaProduto" id="categoriaProduto"> 
					    		@foreach($cats as $cat)
									
					    				<option value="{{ $cat->id }}" @if($cat->id == $produto->categoria_id)selected @endif>{{ $cat->nome }}</option>
					    			
					    			
					    			
					    		@endforeach					    	
					    	</select>
					    </div>
				</div>
				<button type="submit" class="btn btn-primary btn-sn">Salvar</button>
				<button type="submit" class="btn btn-danger btn-sn">Cancel</button>	
			</form>
		</div>
	</div>
@endsection