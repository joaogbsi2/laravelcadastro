@extends('layout.app', ["current" => "produtos"])

@section('body')

	<div class="card border">
		<h5 class="card-title">Cadastro de Produtos</h5>
		<div class="card-body">
			<table class="table table-ordered table-hover" id="tabelaProdutos">
				<thead>
					<tr>
						<th>Codigo</th>
						<th>Produto</th>
						<th>Estoque</th>
						<th>Preço</th>
						<th>Categoria</th>
						<th>Ações</th>
					</tr>
				</thead>
				<tbody>
					
					{{-- <tr>

						<th>{{ $produto->id }}</th>
						<th>{{ $produto->nome }}</th>
						<th>{{ $produto->estoque }}</th>
						<th>{{ $produto->preco }}</th>
						<th>{{ $produto->categoria_id }}</th>
						<th>
							<a href="/produtos/editar/{{ $produto->id }}" class="btn btn-primary btn-sm">Editar</a>
							<a href="/produtos/apagar/{{ $produto->id }}" class="btn btn-danger btn-sm">Apagar</a>
						</th> 
					</tr> --}}
						
				</tbody>
			</table>			
			
		</div>
		<div class="card-footer">
			<button class="btn btn-primary btn-sm" role="button" onClick="novoProduto()">Novo Produto</a>
		</div>
		
	</div>
	<div  class="modal" id="dlgProdutos" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form class="form-horizontal" id="formProduto">
					<div class="modal-header">
						<h5 class="modal-title"></h5>
					</div>
					<div class="modal-body">
						<input type="hidden" id="id" class="form-control">
						<div class="form-group">
							<label for="nomeProduto" class="control-label">Nome do Produto</label>
							<div class="input-group">
								<input type="text" class="form-control" id="nomeProduto" placeholder="Nome do produto">
							</div>
						</div>

						<div class="form-group">
							<label for="estoqueProduto" class="control-label">Estoque</label>
							<div class="input-group">
								<input type="number" class="form-control" id="estoqueProduto" placeholder="Estoque do produto">
							</div>
						</div>

						<div class="form-group">
							<label for="precoProduto" class="control-label">Preço</label>
							<div class="input-group">
								<input type="number" class="form-control" id="precoProduto" placeholder="Preço do produto">
							</div>
						</div>

						<div class="form-group">
							<label for="categoriaProduto" class="control-label">Categoria</label>
							<div class="input-group">
								<select  class="form-control" id="categoriaProduto" >
								</select>
							</div>
						</div>
						
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary">Salvar</button>
						<button type="cancel" class="btn btn-secundary" data-dismiss="modal">Cancelar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection

@section('javascript')

	<script type="text/javascript">

		$.ajaxSetup({
			headers:{
				'X-CSRF-TOKEN': "{{ csrf_token() }}"
			}
		});
		function novoProduto(){
			$('#id').val('');
			$('#nomeProduto').val('');
			$('#estoqueProduto').val('');
			$('#precoProduto').val('');
			$('#categoriaProduto').val('');
			$('#dlgProdutos').modal('show');
		}

		function carregarCategorias(){
			$.getJSON('/api/categorias', function(categorias){
				
				for(i=0;i<categorias.length; i++){
					opcao = '<option value="'+categorias[i].id +'" >'+categorias[i].nome+'</option>';					
					$('#categoriaProduto').append(opcao);
				}
			});
		}

		function montarLinha(produto){
			var linha =
				"<tr>"+ 
					"<td>"+produto.id           +"</td>"+
					"<td>"+produto.nome         +"</td>"+
					"<td>"+produto.estoque      +"</td>"+
					"<td>"+produto.preco        +"</td>"+
					"<td>"+produto.categoria_id +"</td>"+
					"<td>"+
					  '<button class="btn btn-sm btn-primary"> Editar </button>'+
					  '<button class="btn btn-sm btn-danger"> Apagar </button>'+					  
					 "</td>"+
				"</tr>";
			return linha			
		}

		function carregarProdutos(){
			$.getJSON('/api/produtos', function(produtos){
				//console.log(produtos);
				for(i=0; i<produtos.length; i++){
					linha =montarLinha(produtos[i]);							
					$('#tabelaProdutos').append(linha);						
				}
			});
		}

		function criarProduto(){
			prod = {
				nome:         $('#nomeProduto').val(),			
				preco:        $('#precoProduto').val(),
				estoque:      $('#estoqueProduto').val(),
				categoria_id: $('#categoriaProduto').val()
			};
			
			$.post("/api/produtos", prod, function(data){
				produto = JSON.parse(data);
				linha =montarLinha(produto);							
				$('#tabelaProdutos').append(linha);
			});
		}

		$('#formProduto').submit(function(event){
			event.preventDefault();
			criarProduto();
			$('#dlgProdutos').modal('hide');			
		});

		$(function(){
			carregarProdutos();
			carregarCategorias();
		});
	</script>

@endsection

